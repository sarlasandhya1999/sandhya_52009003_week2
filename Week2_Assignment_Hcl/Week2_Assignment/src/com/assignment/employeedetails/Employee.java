package com.assignment.employeedetails;

public class Employee {
	int id;
	String name;
	int age;
	int salary; 
	String department;
	String city;
	  
	//Getter and setters for getting and setting properties  
	public void setEmployeeDetails(int id, String name, int age, int salary, String department, String city) {  
	  try {
		  this.id = id; 
		  if(id<0) {
			  throw new IllegalArgumentException("Employee ID Should Not Be Less Or Equel To Zero");
		  }
	      
		  this.name = name;
	      if(name.isEmpty()||name==null) {
	    	  throw new IllegalArgumentException("Employee Name Should Not Be Empty Or Null");
	      }
	      
	      this.age = age;
	      if(age<0) {
	    	  throw new IllegalArgumentException("Employee Age Should Not Be Less Or Equel To Zero");
	      }
	      
	      this.salary = salary;
	      if(salary<0) {
	    	  throw new IllegalArgumentException("Employee Salary Should Not Be Less Or Equel To Zero");
	      }
	      
	      this.department = department;
	      if(department.isEmpty()||department==null) {
	    	  throw new IllegalArgumentException("Employee Department Should Not Be Empty Or Null");
	      }
	      
	      this.city = city;
	      if(city.isEmpty()||city==null) {
	    	  throw new IllegalArgumentException("City Field Should Not Be Empty Or Null");
	      }
	  }catch(Exception e) {
		  System.out.println(" " + e.getMessage());
	  }
	} 
		
	public int getEmpId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public int getSalary() {
		return salary;
	}	
	
	public String getDept() {
		return department;
	}
	
	public String getCity() {
		return city;
	}
	
	public void getEmployeeDetails() {
		System.out.println(" "+ id + " " + name + " " + age + " " + salary + " " + department + " " + city);
	}
	
}  
	      
